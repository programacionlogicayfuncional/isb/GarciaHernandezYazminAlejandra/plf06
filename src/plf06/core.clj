(ns plf06.core
  (:gen-class))

;;(apply str (range 65 91))
(def characters (str "aábcdeéfghiíjklmnñoópqrstuúüvwxyz01234!" 
                     (apply str (map char (concat (range 34 48) (range 58 65) (range 91 97) (range 123 127)))) "56789"
                     "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ"))
(def character-list (into [] characters))

;;Devuelve el índice del caracter
(defn indice-caracter
  [x]
  (letfn [(f [x]
            (.indexOf character-list x))]
    (f x)))

;;Si está entre 0 y 32 es una letra minúscula
(defn letras
  [x]
  (letfn [(f [x]
            (if (> x 32)
              (- x 33)
              x))]
    (f x)))

;;Si está entre 75 y 107 es una letra mayúscula
(defn letras-mayusculas
  [x]
  (letfn [(f [x]
            (if (> x 106)
              (+ 73 (- x 106))
              x))]
    (f x)))

;;Si está entre 33 y 74 es un símbolo
(defn simbolos
  [x]
  (letfn [(f [x]
            (if (> x 73)
              (+ 31 (- x 73))
              x))]
    (f x)))

;;Determina si es letra o símbolo
(defn pareja-caracter
  [x]
  (letfn [(f [x]
            (cond
              (<= 0 x 32) (letras (+ x 13))
              (<= 33 x 74) (simbolos (+ x 13))
              (<= 75 x 100) (letras-mayusculas (+ x 13))
              :else 0))]
    (f x)))

;;Devuelve la posición del nuevo caracter
(defn nuevo-caracter
  [x]
  (letfn [(f [x]
            (pareja-caracter (indice-caracter x)))]
    (f x)))

;;Devuelve el caracter de la posición solicitada
(defn convierte
  [x]
  (letfn [(f [x]
            (if (= (int x) 32)
              (char 32)
              (get characters (nuevo-caracter x))))]
    (f x)))

;;Hace todos los pasos anteriores con cada caracter de la palabra
(defn cada-caracter
  [xs]
  (letfn [(f [xs]
            (map convierte xs))]
    (f xs)))

;;Convierte el conjunto de caracteres en una palabra
(defn encriptar
  [x]
  (letfn [(f [x]
            (apply str (cada-caracter (into [] x))))]
    (f x)))

;;Función principal
(defn -main
  [& args]
  (if (empty? args)
    (println "Error: 
              Imposible encriptar
              No se puede encontrar el texto")
    (println (encriptar (apply str args)))))

;(-main "c AN CION # 7 2")
;(-main "Canción #72")
;(-main "Canción" " #72")
;(-main "Can" "ción" " #72")
;(-main "Can" "ción" " #" "72")